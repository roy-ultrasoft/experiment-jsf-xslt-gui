package com.r0y.jsfxsltgui.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ch.ultrasoft.jsfxsltgui.transformer.XSLTransformer;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponent;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentAttribute;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentList;

public class XSLTransformerTest {

	private static final Logger LOG = Logger.getLogger(XSLTransformerTest.class);
	private static final String XHTML = "src/test/resources/dummyFields.xhtml";
	private static JSFViewComponentList jSFViewComponents = new JSFViewComponentList(XHTML);
	
	@Before
	public void before() {
		JSFViewComponentAttribute jSFViewComponentAttribute_name = new JSFViewComponentAttribute("disabled", "test-true");
		JSFViewComponentAttribute jSFViewComponentAttribute_vorname = new JSFViewComponentAttribute("disabled", "testII-true");
		List<JSFViewComponentAttribute> jSFViewComponentAttributes_name = new ArrayList<>();
		jSFViewComponentAttributes_name.add(jSFViewComponentAttribute_name);
		jSFViewComponents.getJsfViewComponents().add(new JSFViewComponent("nameIn", jSFViewComponentAttributes_name));
		List<JSFViewComponentAttribute> jSFViewComponentAttributes_vorname = new ArrayList<>();
		jSFViewComponentAttributes_vorname.add(jSFViewComponentAttribute_vorname);
		jSFViewComponents.getJsfViewComponents().add(new JSFViewComponent("vornameIn", jSFViewComponentAttributes_vorname));
	}
	
	@Test
	public void testFromXHTML() {
		StringWriter transformedXML = null;
		try {
			transformedXML = new XSLTransformer().fromXHTML(XHTML, jSFViewComponents);
			LOG.info(transformedXML.toString());
		} catch (Exception e) {
			LOG.error("testFromXHTML", e);
		}
		assertNotNull(transformedXML);
		assertTrue(transformedXML.toString().contains("info=\"p:inputText[@id = nameIn]/disabled = test-true\""));
		assertTrue(transformedXML.toString().contains("info=\"p:inputText[@id = vornameIn]/disabled = testII-true\""));
	}
	
	@Test
	public void testToXHTML() {
		StringWriter transformedXML = null;
		try {
			transformedXML = new XSLTransformer().toXHTML(XHTML, jSFViewComponents);
			LOG.info(transformedXML.toString());
		} catch (Exception e) {
			LOG.error("testToXHTML", e);
		}
		assertNotNull(transformedXML);
		assertTrue(transformedXML.toString().contains("<p:inputText id=\"nameIn\" value=\"#{dummyBean.selected.name}\" required=\"true\" disabled=\"test-true\"></p:inputText>"));
		assertTrue(transformedXML.toString().contains("<p:inputText id=\"vornameIn\" value=\"#{dummyBean.selected.vorname}\" required=\"true\" disabled=\"testII-true\"></p:inputText>"));
		assertTrue(transformedXML.toString().contains("<h:outputLabel id=\"nameLbl\" value=\"name\" for=\"name\"></h:outputLabel>"));
	}
}
