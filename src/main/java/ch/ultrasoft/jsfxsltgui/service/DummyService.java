package ch.ultrasoft.jsfxsltgui.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import ch.ultrasoft.jsfxsltgui.model.Dummy;
import ch.ultrasoft.jsfxsltgui.model.LazyDummyDataScrollModel;

public class DummyService implements Serializable {

	private static final long serialVersionUID = 7904694984508943997L;
	private static final Logger LOG = Logger.getLogger(DummyService.class);
	private long resultSize = 0L;

	@Inject
	private EntityManager em;

	public void createTestDummies(int recordsToCreate) {
		try {
			TransactionHandler.beginTransaction(em);
			Dummy d = new Dummy();
			for (int i = 0; i < recordsToCreate; i++) {
				d.setName(new BigInteger(40, new SecureRandom()).toString(32));
				TransactionHandler.merge(d, em);
				if (i % 10 == 0)
					em.flush();
			}
			TransactionHandler.commitTransaction(em);
			LOG.info(String.format("created %s records", recordsToCreate));
		} catch (Exception e) {
			LOG.error("could not create records", e);
			TransactionHandler.rollbackTransaction(em, e);
		}
		initResultSize();
	}

	public Dummy saveDummy(Dummy dummy) throws Exception {
		TransactionHandler.beginTransaction(em);
		try {
			dummy = TransactionHandler.merge(dummy, em);
			TransactionHandler.commitTransaction(em);
			return dummy;
		} catch (Exception e) {
			TransactionHandler.rollbackTransaction(em, e);
			throw e;
		}
	}

	public LazyDummyDataScrollModel createLazyDummyDataScrollModel() {
		return new LazyDummyDataScrollModel(em);
	}

	private void initResultSize() {
		resultSize = (long) em.createQuery("SELECT COUNT(d) FROM Dummy d").getSingleResult();
	}
	
	public long getResultSize() {
		if (resultSize == 0L)
			initResultSize();
		return resultSize;
	}
}
