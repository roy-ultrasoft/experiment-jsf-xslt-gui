package ch.ultrasoft.jsfxsltgui.service;

import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;

import ch.ultrasoft.jsfxsltgui.bean.NavigationBean;
import ch.ultrasoft.jsfxsltgui.transformer.XSLTransformer;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentAttribute;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentList;
import ch.ultrasoft.jsfxsltgui.view.model.utils.JSFViewModelUtils;

@WebServlet("/ViewEditorServlet")
public class ViewEditorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ViewEditorServlet.class);
	private final String XHTML = "resources/jsf/fragments/fields/dummyFields.xhtml"; // = getServletContext().getRealPath("resources/jsf/fragments/fields/dummyFields.xhtml"); //"src/test/resources/dummyFields.xhtml";
	private static final String PRAEFIX = "TEST";
	
	private JSFViewComponentList jSFViewComponents = null;
	private XSLTransformer xslTransformer = new XSLTransformer();

	@Inject
	private NavigationBean navigationBean;
	
    public ViewEditorServlet() {
        super();
        jSFViewComponents = new JSFViewComponentList(XHTML);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	try {
    		String absolutePathToXHTML = navigationBean.resolveCustomXHTMLAbsolute(XHTML);
			String html = xslTransformer.fromXHTML(absolutePathToXHTML, jSFViewComponents).toString();
			String toXhtml = xslTransformer.toXHTML(absolutePathToXHTML, jSFViewComponents).toString();
			toXhtml = toXhtml.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
			if (toXhtml != null && !toXhtml.isEmpty()) {
				html = html.replaceAll("toXHTML.xsl", toXhtml);
				resp.getWriter().append(html);
			}
			LOG.debug(html);
		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			LOG.error("error XSLTransformer().fromXHTML: " + getServletContext().getRealPath(XHTML), e);
		}
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = request.getParameter("page");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String componentId = request.getParameter("componentId");
		String saveXHTML = request.getParameter("saveXHTML");

		LOG.info(name);
		LOG.info(value);
		LOG.info(page);
		LOG.info(componentId);
		
		if (saveXHTML != null && !saveXHTML.isEmpty()) {
			try {
				String path = navigationBean.getWebappRootAbsolute() + XHTML.replace(".xhtml", "_TEST.xhtml");
				String xhtml = xslTransformer.toXHTML(navigationBean.resolveCustomXHTMLAbsolute(XHTML), jSFViewComponents).toString();
				writeXHTML(path, xhtml);
			} catch (TransformerFactoryConfigurationError | TransformerException e) {
				LOG.error("", e);
			}
		} else {
			JSFViewComponentAttribute attribute = new JSFViewComponentAttribute(name, value); 
			JSFViewModelUtils.addAttribute(jSFViewComponents, attribute, componentId);
		}
		doGet(request, response);
	}
	
	private void writeXHTML(String path, String xhtml) {
		try (FileWriter fileWriter = new FileWriter(path);) {
			fileWriter.write(xhtml);
			fileWriter.flush();
		} catch (IOException e) {
			LOG.error("writeXHTML", e);
		}
	}
}
