package ch.ultrasoft.jsfxsltgui.service;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import ch.ultrasoft.jsfxsltgui.model.Dummy;

public class TransactionHandler implements Serializable {

	private static final long serialVersionUID = 3043223118871259323L;
	private static final Logger LOG = Logger.getLogger(TransactionHandler.class);

	public static void beginTransaction(EntityManager em) {
		em.getTransaction().begin();
	}
	
	public static void rollbackTransaction(EntityManager em, Exception e) {
		LOG.error("rollbackTransaction", e);
		if (em.getTransaction().isActive())
			em.getTransaction().rollback();
	}

	public static void commitTransaction(EntityManager em)  throws Exception {
		em.getTransaction().commit();
		LOG.debug(String.format("commitedTransaction"));
	}

	public static Dummy merge(Dummy dummy, EntityManager em) throws Exception {
		dummy = em.merge(dummy);
		LOG.debug(String.format("mergedTransaction"));
		return dummy;
	}
}
