package ch.ultrasoft.jsfxsltgui.transformer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;

import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponent;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentList;

public class XSLTransformer implements Serializable {

	private static final long serialVersionUID = 4043602214416806352L;
	private static final Logger LOG = Logger.getLogger(XSLTransformer.class);
	private XStream xStream = new XStream();
	
	private static TransformerFactory transformerFactory = TransformerFactory.newInstance();
	
	public StringWriter toXHTML(String xhtml, JSFViewComponentList jSFViewComponents) throws TransformerFactoryConfigurationError, TransformerException, IOException   {
		try (FileInputStream fileInputStream = new FileInputStream(xhtml)) {
			return transform(fileInputStream, "toXHTML.xsl", jSFViewComponents);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public StringWriter fromXHTML(String xhtml, JSFViewComponentList jSFViewComponents) throws TransformerFactoryConfigurationError, TransformerException, IOException   {
		try (FileInputStream fileInputStream = new FileInputStream(xhtml)) {
			return new XSLTransformer().transform(fileInputStream, "fromXHTML.xsl", jSFViewComponents);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public StringWriter transform(FileInputStream inputStream, String xslFile, JSFViewComponentList jSFViewComponents) throws TransformerFactoryConfigurationError, TransformerException, IOException   {
		try {
			String XHTML = appendPropertiesToXHTML(inputStream, jSFViewComponents);
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(XHTML.getBytes());
			LOG.info(XHTML);
			Transformer transformer = transformerFactory.newTransformer(new StreamSource(xslFile));
			StringWriter stringWriter = new StringWriter();
			transformer.transform(new StreamSource(byteArrayInputStream), new StreamResult(stringWriter));
			return stringWriter;
		} catch (TransformerFactoryConfigurationError | TransformerException | IOException e) {
			throw e;
		}
	}
	
	private String appendPropertiesToXHTML(FileInputStream inputStream, JSFViewComponentList jSFViewComponents) throws IOException {
		String xhtmlAsString = getXhtmlAsString(inputStream);
		xStream.aliasPackage("", "ch.ultrasoft.jsfxsltgui.view.model");
		String xmlProperties = xStream.toXML(jSFViewComponents);
		xmlProperties = xmlProperties.replaceAll("</", "</p:");
		xmlProperties = xmlProperties.replaceAll("<(\\b)", "<p:$1");
		LOG.info(xmlProperties);
		return xhtmlAsString.replaceAll("</ui:composition>", xmlProperties + 
				"\n</ui:composition>"); 
	}

	private String getXhtmlAsString(FileInputStream inputStream) throws IOException {
		StringBuilder xhtml = null;
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));){
			char[] buffer = new char[1024];
			int len = 1024;
			xhtml = new StringBuilder();
			while ((len = bufferedReader.read(buffer, 0, len)) != -1) {
				xhtml.append(buffer);
			}
		} catch (IOException e) {
			throw e;
		}
		return xhtml != null ? xhtml.toString().trim() : null;
	}
}
