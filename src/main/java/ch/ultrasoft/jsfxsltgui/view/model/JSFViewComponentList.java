package ch.ultrasoft.jsfxsltgui.view.model;

import java.util.ArrayList;
import java.util.List;

public class JSFViewComponentList {
	private String id = null;
	private List<JSFViewComponent> jsfViewComponents = null;

	public JSFViewComponentList(String id, List<JSFViewComponent> jsfViewComponent) {
		this.id = id;
		this.jsfViewComponents = jsfViewComponent;
	}
	
	public JSFViewComponentList(String id) {
		this(id, new ArrayList<JSFViewComponent>());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<JSFViewComponent> getJsfViewComponents() {
		return jsfViewComponents;
	}

	public void setJsfViewComponents(List<JSFViewComponent> jsfViewComponent) {
		this.jsfViewComponents = jsfViewComponent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((jsfViewComponents == null) ? 0 : jsfViewComponents.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JSFViewComponentList other = (JSFViewComponentList) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (jsfViewComponents == null) {
			if (other.jsfViewComponents != null)
				return false;
		} else if (!jsfViewComponents.equals(other.jsfViewComponents))
			return false;
		return true;
	}
}