package ch.ultrasoft.jsfxsltgui.view.model;

import java.util.ArrayList;
import java.util.List;

public class JSFViewComponent {
	private String id = null;
	private List<JSFViewComponentAttribute> attributes = null;

	public JSFViewComponent(String id, List<JSFViewComponentAttribute> attributes) {
		this.id = id;
		this.attributes = attributes;
	}
	
	public JSFViewComponent(String id) {
		this(id, new ArrayList<JSFViewComponentAttribute>());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<JSFViewComponentAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<JSFViewComponentAttribute> attributes) {
		this.attributes = attributes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JSFViewComponent other = (JSFViewComponent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}