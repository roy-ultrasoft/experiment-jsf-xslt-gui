package ch.ultrasoft.jsfxsltgui.view.model.utils;

import java.util.ArrayList;

import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponent;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentAttribute;
import ch.ultrasoft.jsfxsltgui.view.model.JSFViewComponentList;

public class JSFViewModelUtils {

	public static void addAttribute(JSFViewComponentList componentList, JSFViewComponentAttribute attribute, String componentId) {
//		List<JSFViewComponentAttribute> attributes = new ArrayList<>();
//		attributes.add(attribute);
//		JSFViewComponent component = new JSFViewComponent(componentId, attributes);
//		componentList.getJsfViewComponents().remove(component);
//		componentList.getJsfViewComponents().add(component);
		boolean added = false;
		for (JSFViewComponent jsfViewComponent : new ArrayList<>(componentList.getJsfViewComponents())) {
			if (jsfViewComponent.getId().equals(componentId)) {
				componentList.getJsfViewComponents().remove(jsfViewComponent);
				addAttribute(jsfViewComponent, attribute);
				componentList.getJsfViewComponents().add(jsfViewComponent);
				added = true;
			}
		}
		if (!added) {
			JSFViewComponent component = new JSFViewComponent(componentId);
			component.getAttributes().add(attribute);
			componentList.getJsfViewComponents().add(component);
		}
	}
	
	public static void addAttribute(JSFViewComponent jsfViewComponent, JSFViewComponentAttribute attribute) {
		jsfViewComponent.getAttributes().remove(attribute);
		jsfViewComponent.getAttributes().add(attribute);
	}
}
