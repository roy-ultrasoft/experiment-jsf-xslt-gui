package ch.ultrasoft.jsfxsltgui.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import ch.ultrasoft.jsfxsltgui.model.Dummy;
import ch.ultrasoft.jsfxsltgui.model.LazyDummyDataScrollModel;
import ch.ultrasoft.jsfxsltgui.service.DummyService;

@Named("dummyBean")
@ViewScoped
public class DemoBean implements Serializable {

	private static final long serialVersionUID = -5703244300384213407L;
	private static final Logger LOG = Logger.getLogger(DemoBean.class);

	private Dummy selected = new Dummy();
	private LazyDataModel<Dummy> lazyScrollingModel;
	
	@Inject
	private DummyService service;

	@PostConstruct
	public void init() {
		lazyScrollingModel = service.createLazyDummyDataScrollModel();
	}
	
	public void onRowSelect(SelectEvent event) {
		Dummy d = (Dummy) event.getObject();
		LOG.info(d);
		FacesContext.getCurrentInstance().addMessage(
				"", 
				new FacesMessage("Selected", d.toString()));
	}

	public void createTestDummies(int recordsToCreate) {
		service.createTestDummies(recordsToCreate);
	}
	
	public LazyDataModel<Dummy> getLazyScrollingModel() {
		return lazyScrollingModel;
	}
	
	public void saveSelected() {
		try {
			selected = service.saveDummy(selected);
			FacesContext.getCurrentInstance().addMessage("",new FacesMessage("saved", selected.toString()));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",new FacesMessage(FacesMessage.SEVERITY_ERROR, "save faild", e.getMessage()));
		}
	}
	
	public void create() {
		selected = new Dummy();
	}

	public Dummy getSelected() {
		return selected;
	}

	public void setSelected(Dummy selected) {
		this.selected = selected;
	}

	public long getResultSize() {
		return service.getResultSize();
	}

}
