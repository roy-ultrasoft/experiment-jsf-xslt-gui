package ch.ultrasoft.jsfxsltgui.bean;

import java.io.File;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;

@Named("navigationBean")
@ApplicationScoped
public class NavigationBean implements Serializable {

	private static final long serialVersionUID = 6857667633998234386L;
	private String webappRootAbsolute;
	
	@PostConstruct
	public void init() {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
	            .getExternalContext().getContext();
		webappRootAbsolute = ctx.getRealPath("/");
	}

	public String resolveCustomXHTMLRelative(String defaultPath) {
		String customFile = defaultPath.replaceAll(".xhtml", "_TEST.xhtml");
		if (new File(webappRootAbsolute + customFile).exists())
			return customFile;
		return defaultPath;
	}

	public String resolveCustomXHTMLAbsolute(String defaultPath) {
		String customFile = defaultPath.replaceAll(".xhtml", "_TEST.xhtml");
		if (new File(webappRootAbsolute + customFile).exists())
			return webappRootAbsolute + customFile;
		return webappRootAbsolute + defaultPath;
	}
	
	public String getWebappRootAbsolute() {
		return webappRootAbsolute;
	}
}
