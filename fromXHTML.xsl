<?xml version="1.0" encoding="UTF-8"?>

<!-- New XSLT document created with EditiX XML Editor (http://www.editix.com) at Sun May 14 00:20:00 CEST 2017 -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:p="http://primefaces.org/ui">
	<xsl:output method="html"/>
	<xsl:template match="ui:composition">
		<html>
			<head><!-- Latest compiled and minified CSS -->
							
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				
				<script>
  					function post(name, value) {
  						console.log("name: " + name);
  						console.log("value: " + value);
	  					$.post("http://localhost:8080/ViewEditorServlet", { 
							name : name, 
							value : value,  
							form : "CurentForm.xhtml" 
						}, function(data) {
  							console.log("data: " + data);
						}); 
					}
  				</script>
			</head>
			<body>
				<center>
					<h1>
						<a class="btn btn-default btn-lg" href="/">Home</a>
							XSL - JSF GUI
						<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">show XHTML</button>
					</h1>
				</center>
				<div class="container">
					
					  <!-- Modal -->
					  <div class="modal fade" id="myModal" role="dialog">
					    <div class="modal-dialog modal-lg">
					    
					      <!-- Modal content-->
					      <div class="modal-content">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal">X</button>
					          <h4 class="modal-title">Modal Header</h4>
					        </div>
					        <div class="modal-body">
					          <pre style="font-weigth:bold;font-size:15px;">toXHTML.xsl</pre>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					          <form action="" method="POST">
					          	<input type="hidden" name="saveXHTML" value="true" />
					          	<button type="submit" class="btn btn-default">Save</button>
					          </form>
					        </div>
					      </div>
					      
					    </div>
					  </div>  
					          
					  
					<xsl:apply-templates select="child::node()[not(starts-with(local-name(),'JSFViewComponentList'))] "/>
				</div>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="ui:composition//*">
		<ul>
			<li>
				<h3>
					<xsl:value-of select="concat('&lt;b&gt;',@id, '&lt;/b&gt; (',name(), ')')" disable-output-escaping="yes"/>
				</h3>
				<xsl:apply-templates select="." mode="defineAttributes"/>
				<xsl:apply-templates select="child::node()"/>
			</li>
		</ul>
	</xsl:template>
	<xsl:template match="*" mode="defineAttributes">
		<ul>
			<div class="fluid-container">
				<div class="row">
					<xsl:variable name="id" select="@id"/>
					<xsl:for-each select="@*">
						<xsl:variable name="name" select="name()"/>
						<xsl:variable name="value" select="."/>
						<!-- <xsl:variable name="newValue" select="/*//p:properties/p:entry[p:id = $id]/p:attribute[p:id = $name]/p:value"/> -->
						<xsl:variable name="newValue" select="/*//p:JSFViewComponentList/p:jsfViewComponents/p:JSFViewComponent[p:id = $id]/p:attributes/p:JSFViewComponentAttribute[p:id = $name]/p:value" />
						<div class="col-md-6 well well-sm">
							<form action="" method="POST">
								<div class="col-md-2">
									<h5 for="{$name}">
										<xsl:value-of select="concat('&lt;b&gt;',$name, '&lt;/b&gt;')" disable-output-escaping="yes"/>
										<input type="hidden" name="name" value="{$name}" />
										<input type="hidden" name="page" />
										<input type="hidden" name="componentId" value="{$id}" />
									</h5>
								</div>
								<div class="col-md-8">
									<xsl:choose>
										<xsl:when  test="$newValue">
											<input type="text" class="form-control" id="{$name}" name="value" value="{$newValue}" info="{name(..)}[@id = {parent::*/@id}]/{$name} = {$newValue}"/>
										</xsl:when>
										<xsl:otherwise>
											<input type="text" class="form-control" id="{$name}" name="value" value="{$value}" info="{name(..)}[@id = {parent::*/@id}]/{$name} = {$value}" />
										</xsl:otherwise>
									</xsl:choose>
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-default" aria-label="save">
										<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"/>
									</button>
								</div>
							</form>
						</div>
					</xsl:for-each>
					
					<!-- Geht  mit aktueller XSL struktur nicht 
					<div class="col-md-6 well well-sm" style="background: rgb(217, 237, 247);">
						<form action="" method="POST">
							<div class="col-md-5">
								<input type="text" class="form-control" id="newId" name="name" info="newId"/>								
								<input type="hidden" name="componentId" value="{$id}" />
							</div>
							<div class="col-md-5">
								<input type="text" class="form-control" id="newValue" name="value" info="newValue"/>
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-default" aria-label="save">
									<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"/>
								</button>
							</div>
						</form>
					</div> -->
				</div>
			</div>
		</ul>
	</xsl:template>
</xsl:stylesheet>