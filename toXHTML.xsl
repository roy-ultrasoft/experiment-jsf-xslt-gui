<?xml version="1.0"?>
<xsl:stylesheet version="1.0"  xmlns:exsl="http://exslt.org/common" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:p="http://primefaces.org/ui">
	<xsl:output omit-xml-declaration="yes" indent="yes"/>
	<xsl:variable name="propertiesNodeName" select="'properties'" ></xsl:variable>
	<xsl:template match="ui:composition" >
		<xsl:copy>
			<xsl:apply-templates  select="child::node()[not(starts-with(local-name(),'JSFViewComponentList'))] "></xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="ui:composition//*">
		<xsl:value-of select="concat('&lt;',name())" disable-output-escaping="yes" />
		<xsl:apply-templates  select="." mode="defineAttributes" />
		<xsl:value-of select="'&gt;'" disable-output-escaping="yes"  />
		<xsl:apply-templates  select="child::node()"></xsl:apply-templates>
		<xsl:value-of select="concat('&lt;/',name(), '>')" disable-output-escaping="yes" />
	</xsl:template>
	
	<xsl:template match="*" mode="defineAttributes" >
		
		<xsl:variable name="id" select="@id"></xsl:variable>
		<xsl:for-each select="@*">
			<xsl:variable name="name" select="name()" />
			<xsl:variable name="value" select="." />
			<!-- <xsl:variable name="newValue" select="/*//p:properties/p:entry[p:id = $id]/p:attribute[p:id = $name]/p:value" /> -->
			<xsl:variable name="newValue" select="/*//p:JSFViewComponentList/p:jsfViewComponents/p:JSFViewComponent[p:id = $id]/p:attributes/p:JSFViewComponentAttribute[p:id = $name]/p:value" />
			<xsl:choose>
				<xsl:when  test="$newValue">
					<xsl:value-of select="concat(' ',$name, '=&quot;',$newValue,'&quot;')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat(' ',$name, '=&quot;',$value,'&quot;')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>	
	</xsl:template>
</xsl:stylesheet>